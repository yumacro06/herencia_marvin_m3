/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia_empleados;

/**
 *
 * @author elmar
 */
public class Herencia_empleados {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        List<Empleado> lista = new List<>();
        
        Empleado empleado1 = new Empleado("Alfonso");
        Directivo empleado2 = new Directivo("Maria");
        Operario empleado3 = new Operario("Juan");
        Oficial empleado4 = new Oficial("Laura");
        Tecnico empleado5 = new Tecnico("Pau");
        
        lista.add(empleado1);
        lista.add(empleado2);
        lista.add(empleado3);
        lista.add(empleado4);
        lista.add(empleado5);
        
        List<Operario> operarios = new List<>();
        
        for (Empleado emp: lista){
            if(emp instanceof Operario){
                operarios.add((Operario) emp);
            }
        }
        
        System.out.println(lista);
        System.out.println(operarios);
        
        System.out.println(empleado1);
        System.out.println(empleado2);
        System.out.println(empleado3);
        System.out.println(empleado4);
        System.out.println(empleado5);
        
        
        
    }
    
}
