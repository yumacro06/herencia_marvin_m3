/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia_empleados;

import java.util.ArrayList;

/**
 *
 * @author elmar
 */
public class List<E> extends ArrayList<E>{
    
    @Override
    public boolean add(E e) {
        boolean afegit=false;       
        
        if(!contains(e)){
            super.add(e);//cridem al mètode add de la superclasse
            afegit=true;
        }
        return afegit;
    }
    
}
