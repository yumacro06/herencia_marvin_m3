/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia_empleados;

/**
 *
 * @author elmar
 */
public class Empleado{

    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public Empleado(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Empleado " + nombre;
    }
    
    
}
